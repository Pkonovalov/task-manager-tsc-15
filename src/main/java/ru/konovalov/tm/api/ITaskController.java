package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

public interface ITaskController {
    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskById();

    void showTask(Task task);

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    void changeTaskStatusById();

    void changeTaskStatusByName();

    void changeTaskStatusByIndex();

    void assignTaskByProjectId();

    void unassignTaskById();

    void removeAllTaskByProjectId();

    void showTaskByProjId();
}
