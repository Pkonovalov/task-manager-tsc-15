package ru.konovalov.tm.exeption.system;


import ru.konovalov.tm.constant.TerminalConst;
import ru.konovalov.tm.exeption.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use " + TerminalConst.CMD_HELP + ", it display list of terminal commands.");
    }

}
